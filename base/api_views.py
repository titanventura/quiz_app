import json

from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView

from .models import Quiz, Question, Option
from .serializers import QuizSerializer,QuestionSerializer
from rest_framework.permissions import IsAuthenticated
from users.permissions import InstructorAccessRequiredMixin


class CreateQuizAPI(InstructorAccessRequiredMixin,CreateAPIView):
    serializer_class = QuizSerializer
    queryset = Quiz.objects.all()


class CreateQuestion(InstructorAccessRequiredMixin,APIView):

    def post(self,request):
        payload = json.loads(self.request.data)
        question = payload['question']
        options = payload['options']

        q = Question(**question)
        q.save()

        for option in options:
            o = Option(question=q,**option)
            o.save()

        Option.objects.bulk_create([Option(question=q,**option) for option in options])
        data = QuestionSerializer(q).data
        return JsonResponse({'success':True,'data':data})