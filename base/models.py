from django.db import models
from django.utils import timezone

from users.models import Instructor, Student, Course, ClassGroup


class Quiz(models.Model):
    def __str__(self):
        return self.title

    title = models.CharField(max_length=30)
    total_pts = models.DecimalField(max_digits=5, decimal_places=2)
    created_by = models.ForeignKey(Instructor, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now)
    starts_at = models.DateTimeField()
    ends_at = models.DateTimeField()
    is_open = models.BooleanField(default=True)
    related_course = models.ForeignKey(Course, null=True, blank=True, on_delete=models.CASCADE)
    related_classes = models.ManyToManyField(ClassGroup, null=True, blank=True)
    shuffle_order = models.BooleanField(default=True)


class Question(models.Model):
    def __str__(self):
        return self.question

    question = models.TextField()
    ko_lvl = models.CharField(max_length=3, choices=(
        ('1', 'KO1'), ('2', 'KO2'), ('3', 'KO3'), ('4', 'KO4'), ('5', 'KO5'), ('6', 'KO6'), ('7', 'KO7')))
    co_lvl = models.CharField(max_length=3, choices=(
        ('1', 'CO1'), ('2', 'CO2'), ('3', 'CO3'), ('4', 'CO4'), ('5', 'CO5'), ('6', 'CO6')))
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    points = models.DecimalField(max_digits=5, decimal_places=2)


class Option(models.Model):
    def __str__(self):
        return self.statement

    statement = models.TextField()
    question = models.ForeignKey(Question, models.CASCADE)
    is_correct = models.BooleanField(default=False)


class ScoreCard(models.Model):
    def __str__(self):
        return self.student.profile.roll_no

    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    started_at = models.DateTimeField()
    ended_at = models.DateTimeField()
    questions = models.ManyToManyField(Question, blank=True)
    answers = models.ManyToManyField(Option, blank=True)
    points_scored = models.DecimalField(max_digits=5, decimal_places=2)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
