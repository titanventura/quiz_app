from rest_framework.serializers import ModelSerializer

from base.models import Quiz, Question


class QuizSerializer(ModelSerializer):
    class Meta:
        model = Quiz
        fields = '__all__'


class QuestionSerializer(ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'
        depth = 2
