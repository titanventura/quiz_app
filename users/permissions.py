from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator

from .models import *


def check_instructor(user):
    return user.is_authenticated and (user.is_superuser or user.profile.get_user_type_object()[0] == 'instructor')


class InstructorAccessRequiredMixin(object):
    @method_decorator(user_passes_test(check_instructor))
    def dispatch(self, *args, **kwargs):
        return super(InstructorAccessRequiredMixin, self).dispatch(*args, **kwargs)
