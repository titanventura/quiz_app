from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):

    def __str__(self):
        return self.roll_no

    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True)
    name = models.CharField(max_length=60)
    roll_no = models.CharField(max_length=10)

    def get_user_type_object(self):
        if hasattr(self, 'student'):
            return 'student', self.student
        elif hasattr(self, 'instructor'):
            return 'instructor', self.instructor
        elif hasattr(self, 'department'):
            return 'department', self.department
        elif hasattr(self, 'admin'):
            return 'admin', self.admin
        else:
            return None, None


class Department(models.Model):

    def __str__(self):
        return self.short_code

    profile = models.OneToOneField(Profile, on_delete=models.CASCADE, blank=True)
    name = models.CharField(max_length=50)
    short_code = models.CharField(max_length=5)


class Instructor(models.Model):
    def __str__(self):
        return self.profile.roll_no

    profile = models.OneToOneField(Profile, on_delete=models.CASCADE, blank=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True)


class Admin(models.Model):
    def __str__(self):
        return self.profile.roll_no

    profile = models.OneToOneField(Profile, on_delete=models.CASCADE, blank=True)


class ClassGroup(models.Model):
    def __str__(self):
        return self.batch + ' ' + self.section

    section = models.CharField(max_length=1, choices=(('A', 'A'), ('B', 'B'), ('C', 'C')))
    batch = models.CharField(max_length=5)


class Course(models.Model):
    def __str__(self):
        return self.code + '-' + self.title

    title = models.CharField(max_length=20)
    code = models.CharField(max_length=10)
    related_instructors = models.ManyToManyField(Instructor, blank=True)


class Student(models.Model):
    def __str__(self):
        return self.profile.roll_no

    profile = models.OneToOneField(Profile, on_delete=models.CASCADE, blank=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True)
    class_group = models.ForeignKey(ClassGroup, on_delete=models.CASCADE, blank=True)
    registered_courses = models.ManyToManyField(Course, blank=True)
