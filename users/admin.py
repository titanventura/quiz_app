from django.contrib import admin
from .models import *

admin.site.register(Profile)
admin.site.register(Student)
admin.site.register(Instructor)
admin.site.register(Admin)
admin.site.register(Department)
admin.site.register(ClassGroup)
admin.site.register(Course)